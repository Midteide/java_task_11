
public class Task11 {
    public static void main( String[] args ) {
        int height = Integer.parseInt(args[0]);
        
        for (int i=0; i < height ; i++ ) {  // Loop through "height" in Y-direction
            System.out.printf("#"); // Print rectangle left border with row of "#"
            for (int j=1; j < height-1 ; j++ ) { // For each Y-directon, loop through X-direction, except first and last which always should be a row of "#"
                if ((i == 0) || (i == height-1) ) System.out.printf("#"); // Print top and bottom border of "#"
                else System.out.printf(" "); // Fill rectangle with space
            }
            System.out.println("#"); // Print rectangle right border with row of "#"

        }

    }

}